console.log("Hello World!");

let firstName = `Kaiser`;
let lastName = `Tabuada`;
let age = 28;

let hobbies = [
		"Deep sea jigging (fishing)",
		"On-shore casting (fishing)",
		"Biking",
		"Playing musical instruments",
		"Vlogging",
];

let workAddress = {
	houseNumber: "Block 30 Lot 8",
	streetName: "San Sebastian Street",
	subdivision: "LJ Ledesma Subdivision Phase 2",
	barangay: "Brgy. Buhang",
	district: "Jaro",
	city: "Iloilo City",
	province: "Iloilo",
}

console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(workAddress);

function printUserInfo() {
	let info = `${firstName} ${lastName} is ${age} year of age.`
	console.log(info);
	console.log(`This was printed inside printUserInfo function:`);
	console.log(hobbies);
	console.log(`This was printed inside printUserInfo function:`);
	console.log(workAddress);
}

function civilStatus() {
	let isMarried = false;
	return isMarried;
}

printUserInfo();
console.log("The value of isMarried is: " + civilStatus());